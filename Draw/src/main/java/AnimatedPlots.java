 /**
 * Created by quanl on 9/15/2015.
 */
import java.util.*;

public class AnimatedPlots {
    public static void main(String[] args) {
        // set the scale of the cordinate system
        StdDraw.setXscale(0.0, 200.0);
        StdDraw.setYscale(0.0, 600.0);

        // insert the number of colum
        int n = Integer.parseInt(args[0]);

        // array stores the values of colums
        double h[];
        h = new double[n];

        // insert elements for array
        for(int i=0; i<n; i++){
            h[i] = StdIn.readDouble();
        }

        StdDraw.clear(StdDraw.BLACK);
        //Initialize x
        double x = 200.0/(2*n); //no space between two colums
        //draw the first m colums
        for(int i=0; i<n; i++) {
            StdDraw.setPenColor(StdDraw.GREEN);
            StdDraw.filledRectangle(x, h[i] / 2, 200.0/(n*2), h[i] / 2);
            x=x+200.0/n;
        }
        StdDraw.show(100);

        //main animation loop
        while(true){
            StdDraw.clear(StdDraw.BLACK);
            //move the colum to the left
            double temp;
            temp = StdIn.readDouble();
            for(int i=0; i<n-1; i++){
                h[i]=h[i+1];
            }
            h[n-1]=temp;
            //draw the change
            x = 200.0/(2*n);
            for(int i=0; i<n; i++) {
                StdDraw.setPenColor(StdDraw.GREEN);
                StdDraw.filledRectangle(x, h[i] / 2, 200.0/(n*2), h[i] / 2);
                x=x+200.0/n;
            }
            StdDraw.show(100);
        }

    }
}
